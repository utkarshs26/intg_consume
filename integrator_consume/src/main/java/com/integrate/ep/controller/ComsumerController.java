package com.integrate.ep.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.integrate.ep.domain.order.Order;
import com.integrate.ep.service.OrderService;

@RestController
@RequestMapping("/consumer")
public class ComsumerController {
	
	@Autowired
	OrderService orderService;
	
    @GetMapping( "/getOrder")	
    public Order getOrder(@RequestBody String tibcoOrderId) {
        return orderService.getOrder(tibcoOrderId);
    }
    
    @GetMapping( "/getJmsOrder")	
    public Order getJmsOrder(@RequestBody String tibcoOrderId) {
        return orderService.getJmsOrder(tibcoOrderId);
    }
    
    @PostMapping( "/placeOrder")	
    public String placeOrder(@RequestBody Order order) {
        String tibCoOrderId = orderService.placeOrder(order);
               
        System.out.println("tibCoOrderId = " + tibCoOrderId);
        return tibCoOrderId;
    }
    
    @PostMapping("/placeJmsOrder")    
    public String placeJmsOrder (@RequestBody Order order) {
    	String tibCoOrderId =  "2222";
    	
    	orderService.placeJmsOrder(order);
        
		 System.out.println("tibCoOrderId = " + tibCoOrderId);
		 return tibCoOrderId;
    }

}
