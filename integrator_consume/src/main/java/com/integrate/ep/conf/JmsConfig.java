package com.integrate.ep.conf;

import javax.jms.Message;
import javax.jms.MessageListener;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

@Configuration
public class JmsConfig implements MessageListener {

	String BROKER_URL = "tcp://localhost:61616"; 
	String BROKER_USERNAME = "admin"; 
	String BROKER_PASSWORD = "admin";
	
	@Bean
	public ActiveMQConnectionFactory connectionFactory(){
	    ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
	    connectionFactory.setBrokerURL(BROKER_URL);
//	    connectionFactory.setPassword(BROKER_USERNAME);
//	    connectionFactory.setUserName(BROKER_PASSWORD);
	    return connectionFactory;
	}

/*	@Bean
	public JmsTemplate jmsTemplate(){
	    JmsTemplate template = new JmsTemplate();
	    template.setConnectionFactory(connectionFactory());
	    return template;
	}*/
	
	public static JmsTemplate getJmsTemplate() {
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
	    connectionFactory.setBrokerURL("tcp://localhost:61616");
//	    connectionFactory.setPassword(BROKER_USERNAME);
//	    connectionFactory.setUserName(BROKER_PASSWORD);
		
		JmsTemplate template = new JmsTemplate();
	    template.setConnectionFactory(connectionFactory);
	    return template;
	}


	@Bean
	public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
	    DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
	    factory.setConnectionFactory(connectionFactory());
	    factory.setConcurrency("1-1");
	    return factory;
	}

	@Override
	public void onMessage(Message message) {
		// TODO Auto-generated method stub
		
	}

}