package com.integrate.ep.domain.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.integrate.ep.domain.customer.Customer;
import com.integrate.ep.domain.customer.CustomerAddress;
import com.integrate.ep.domain.product.Product;

public class Order implements Serializable {
	
	/**
	 * Serial version id.
	 */
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 5000000001L;
	
	private String orderNumber;

	private Date createdDate;

	private Date lastModifiedDate;

	private Customer customer;

	private CustomerAddress orderBillingAddress;

	private Locale locale;

	private Currency currency;

	private BigDecimal total = BigDecimal.ZERO;
	
	private List<Product> products;

	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber() {
		return orderNumber;
	}

	/**
	 * @param orderNumber the orderNumber to set
	 */
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the lastModifiedDate
	 */
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	/**
	 * @param lastModifiedDate the lastModifiedDate to set
	 */
	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * @return the orderBillingAddress
	 */
	public CustomerAddress getOrderBillingAddress() {
		return orderBillingAddress;
	}

	/**
	 * @param orderBillingAddress the orderBillingAddress to set
	 */
	public void setOrderBillingAddress(CustomerAddress orderBillingAddress) {
		this.orderBillingAddress = orderBillingAddress;
	}

	/**
	 * @return the locale
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * @param locale the locale to set
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	/**
	 * @return the currency
	 */
	public Currency getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	/**
	 * @return the total
	 */
	public BigDecimal getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	/**
	 * @return the products
	 */
	public List<Product> getProducts() {
		return products;
	}

	/**
	 * @param products the products to set
	 */
	public void setProducts(List<Product> products) {
		this.products = products;
	}

}
