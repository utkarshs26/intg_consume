package com.integrate.ep.service.impl;

import java.util.Arrays;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.integrate.ep.domain.order.Order;
import com.integrate.ep.service.MessageService;
import com.integrate.ep.service.OrderService;

@Service
//@Transactional
public class OrderServiceImpl implements OrderService {
 
	@Override
	public Order getOrder(String tibcoOrderId) {

        RestTemplate restTemplate = new RestTemplate();
        Order order = restTemplate.getForObject("http://localhost:8095/integrate/getOrder", Order.class);
		
		return order;
	}
	
	@Override
	public Order getJmsOrder(String tibcoOrderId) {

        RestTemplate restTemplate = new RestTemplate();
        Order order = restTemplate.getForObject("http://localhost:8095/integrate/getOrder", Order.class);
		
		return order;
	}

    @Override
    public String placeOrder(Order order) {
    	

    	final String uri = "http://localhost:8095/integrate/publishOrder";
        
        RestTemplate restTemplate = new RestTemplate();
         
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
    	headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Order> entity = new HttpEntity<Order>(order, headers);
         
        ResponseEntity<String> tibcoOrderId = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
        
        return tibcoOrderId.getBody();
    }
    
    @Override
    public void placeJmsOrder(Order order) {
    	MessageService messageService = new MessageService();
    	messageService.sendMessage(order);
    	
//    	return null;
    }
    
}
