package com.integrate.ep.service;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import com.integrate.ep.conf.JmsConfig;
import com.integrate.ep.domain.order.Order;

/**
 * A service that sends and receives JMS messages. 
 * 
 */
public class MessageService 
{

//	@Autowired
//	private JmsTemplate jmsTemplate;
	
//	@Value("OrderQueue")
//	private String destination;
	private final String destination = "OrderQueue";

	/**
	 * Sends a message to a queue.
	 * 
	 * @param text Message text.
	 */
	public void sendMessage(final Order order) {
		JmsConfig.getJmsTemplate().send(destination, new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				return session.createObjectMessage(order);
			}
		});
	}
	
	/**
	 * Receives a message from a queue.
	 * 
	 * @return Order.
	 * @throws JMSException
	 */
	public Order readMessage() throws JMSException {
		Order order = null;

		Message msg = JmsConfig.getJmsTemplate().receive(destination);
		if(msg instanceof Order) {
			order = (Order) msg;
		}
		
		return order;
	}
}
