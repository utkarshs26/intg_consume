package com.integrate.ep.service;

import com.integrate.ep.domain.order.Order;

public interface OrderService {
	Order getOrder (String tibcoOrderId);
	Order getJmsOrder (String tibcoOrderId);
	
	String placeOrder(Order order);
	void placeJmsOrder(Order order);

}
